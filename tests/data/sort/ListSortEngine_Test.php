<?php
require_once dirname(__FILE__) . '/../../../data/sort/ListSortEngine.php';
require_once dirname(__FILE__) . '/../../../data/sort/ListSort.php';

class ListSortEngine_Test extends PHPUnit_Framework_TestCase {

    public function testSort() {
        $list = array('A', 'B', 'C');
        $newList = $list;

        ListSortEngine::context(null)
            ->load('ListSortReverse')->load('ListSortShuffle')
            ->sort($newList);

        // 排序后，数量应当不变； 且一个也不能少，一个也不能多
        $this->assertCount(3, $newList);
        $this->assertCount(0, array_diff($list, $newList));
    }

    public function testSortAlone() {
        $list = array('A', 'B', 'C');

        ListSortEngine::context(null)->load('ListSortReverse')->sort($list);

        $this->assertEquals(array('C', 'B', 'A'), $list);
    }
}


// 逆转排序
class ListSortReverse extends ListSort {
    public function sort($list) {
        return array_reverse($list);
    }
}

// 随机排序
class ListSortShuffle extends ListSort {
    public function sort($list) {
        shuffle($list);
        return $list;
    }
}

