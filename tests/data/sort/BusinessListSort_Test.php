<?php
require_once dirname(__FILE__) . '/../../../data/sort/BusinessListSort.php';

class BusinessListSort_Test extends PHPUnit_Framework_TestCase {

    public function testSort() {
        // 第一步：构建
        $list = array(
            array('id' => 21, 'score' => 7, 'weight' => 90),
            array('id' => 22, 'score' => 8, 'weight' => 80),
            array('id' => 23, 'score' => 9, 'weight' => 70),
        );

        // 第二步：操作
        $plugin = new BusinessListSort(null);
        $newList = $plugin->sort($list);

        // 第三步：检验
        $this->assertEquals(23, $newList[0]['id']);
        $this->assertEquals(22, $newList[1]['id']);
        $this->assertEquals(21, $newList[2]['id']);
    }
}
