<?php
require_once dirname(__FILE__) . '/../../../data/ListQuery.php';
require_once dirname(__FILE__) . '/../../../data/sort/TopListSort.php';

class BusinessListSort_Test extends PHPUnit_Framework_TestCase {

    public function testSort() {
        // 第一步：构建
        $list = array(
            array('id' => 21),
            array('id' => 22),
            array('id' => 23),
        );

        $_GET['type'] = 'php';
        $_GET['top_id'] = 22;

        // 第二步：操作
        $query = new ListQuery();

        $plugin = new TopListSort($query);
        $newList = $plugin->sort($list);

        // 第三步：检验
        $this->assertEquals(22, $newList[0]['id']);
    }
}
