<?php
/**
 * 对首页数据进行提前预热
 */

require_once dirname(__FILE__) . '/../data/IndexQuery.php';
require_once dirname(__FILE__) . '/../data/AdData.php';
require_once dirname(__FILE__) . '/../data/CategoryData.php';

// 穿越到未来，往前推移10分钟
$_SERVER['REQUEST_TIME'] = isset($argv[1]) ? strtotime($argv[1]) : time() + 600;

// 全部城市，例如只有广州gz和深圳sz
$allCitys = array('gz', 'sz');

// 待提前预热的数据
$dataList = array(
    new AdData(),       // 顶部广告
    new CategoryData(), // 职位分类
);

foreach ($allCitys as $city) {
    // 首页查询参数对象
    $query = new IndexQuery();
    // 指定所在城市
    $query->city = $city;

    // 不读缓存，但提前写入缓存
    $query->cacheRead = false;
    $query->cacheWrite = true;

    // 提前预热数据
    foreach ($dataList as $obj) {
        $obj->getData($query);
    }
}

echo 'done!', PHP_EOL;

