<?php
require_once dirname(__FILE__) . '/dataprovider/LightWeightDataProvider.php';
require_once dirname(__FILE__) . '/../model/DetailModel.php';

class MoreDetailData extends LightWeightDataProvider {

    protected function doGetData(DataQuery $query, &$trace = '') {
        // 全球追踪器：数据库
        $trace .= 'D';

        $model = new DetailModel();
        return $model->getMoreDetailData($query->id);
    }

    protected function getCacheKey(DataQuery $query) {
        return 'job_detail_more_' . $query->id;
    }
}
