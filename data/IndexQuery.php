<?php
require_once dirname(__FILE__) . '/dataprovider/DataQuery.php';

class IndexQuery extends DataQuery {
    // 所在城市，如：gz＝广州，sz=深圳
    public $city;

    public function __construct() {
        // 默认城市
        $this->city = 'gz';

        // 用户真实选择的城市
        if (!empty($_COOKIE['city'])) {
            $this->city = $_COOKIE['city'];
        }

        // 预览时指定的城市
        if (!empty($_GET['city'])) {
            $this->city = $_GET['city'];
        }

        parent::__construct();
    }
}
