<?php

require_once dirname(__FILE__) . '/LightWeightDataProvider.php';

class UserData extends LightWeightDataProvider {
    
    protected function doGetData(DataQuery $query, &$trace = '') {
        // 全球追踪器：数据库
        $trace .= 'D';

        $model = new User();
        return $model->getUserInfo($query->userId);
    }

    protected function getCacheKey(DataQuery $query) {
        return 'userinfo_' . $query->userId;
    }
}

class User {

    public function getUserInfo($userId) {
        return array('name' => 'dogstar');
    }
}

class UserInfoDataQuery extends DataQuery {
    public $userId;
}

$user = new UserData();
$query = new UserInfoDataQuery();

$query->userId = 1;
$trace = '|UserData-'; // 全球追踪器：用户信息（起点）
$userInfo = $user->getData($query, $trace);

var_dump($userInfo);
var_dump($trace);

