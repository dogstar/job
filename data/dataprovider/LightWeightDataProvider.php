<?php
require_once dirname(__FILE__) . '/DataProvider.php';

abstract class LightWeightDataProvider extends DataProvider {

    public function getData(DataQuery $query, &$trace = '') {
        // 全球追踪器：轻量级数据模板
        $trace .= 'L';

        return parent::getData($query, $trace);
    }

    protected function getCacheInstance() {
        return new LightWeightCache(); 
    }

    protected function getCacheExpireTime(DataQuery $query) {
        return 1200;
    }
}

require_once dirname(__FILE__) . '/FileCache.php';

class LightWeightCache extends FileCache {
    public function __construct() {
        parent::__construct(array('path' => dirname(__FILE__) . '/../../'));
    }

    /**
    public function get($key) {
        return NULL;
    }

    public function set($key, $data, $time) {
    }
    **/
}
