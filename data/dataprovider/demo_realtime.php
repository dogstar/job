<?php

require_once dirname(__FILE__) . '/RealTimeDataBinder.php';

class UserInfo extends RealTimeDataBinder {
    
    protected function doGetData(DataQuery $query, &$trace = '') {
        $model = new User();
        return $model->getUserInfo($query->userId);
    }

}

class User {

    public function getUserInfo($userId) {
        return array('name' => 'dogstar');
    }
}

class UserInfoDataQuery extends DataQuery {
    public $userId;
}

$demo = new UserInfo();
$query = new UserInfoDataQuery();

$query->userId = 1;
$userInfo = $demo->getData($query);

var_dump($userInfo);

