<?php
require_once dirname(__FILE__) . '/DataProvider.php';

abstract class RealTimeDataProvider extends DataProvider {

    protected function getCacheInstance() {
        return new NoneCache(); 
    }

    protected function getCacheKey(DataQuery $query) {
        return '';
    }

    protected function getCacheExpireTime(DataQuery $query) {
        return 0;
    }
}

class NoneCache {
    public function get($key) {
        return NULL;
    }

    public function set($key, $data, $time) {
    }
}
