<?php

class DataQuery {

    public $cacheWrite = true;     // 缓存读开关
    public $cacheRead = true;      // 缓存写开关
    public $nowTime;               // 当前请求时间戳

    public function __construct() {
        if (isset($_GET['cacheWrite'])) {
            $this->cacheWrite = $_GET['cacheWrite'] == 0 ? false : true;
        }

        if (isset($_GET['cacheRead'])) {
            $this->cacheRead = $_GET['cacheRead'] == 0 ? false : true;
        }

        // 默认＆预览初始化
        $this->nowTime = $_SERVER['REQUEST_TIME'];
        if (isset($_GET['nowTime'])) {
            $this->nowTime = $_GET['nowTime'];
        }
    }

    public function __set($name, $value) {
        $this->$name = $value;
    }

    public function __get($name) {
        return isset($this->$name) ? $this->$name : NULL;
    }
}
