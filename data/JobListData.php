<?php
require_once dirname(__FILE__) . '/dataprovider/LightWeightDataProvider.php';
require_once dirname(__FILE__) . '/../model/JobModel.php';
require_once dirname(__FILE__) . '/../data/sort/ListSortEngine.php';
require_once dirname(__FILE__) . '/../data/sort/BusinessListSort.php';
require_once dirname(__FILE__) . '/../data/sort/RecommendListSort.php';
require_once dirname(__FILE__) . '/../data/sort/TopListSort.php';

class JobListData extends LightWeightDataProvider {

    protected function doGetData(DataQuery $query, &$trace = '') {
        // 全球追踪器：数据库
        $trace .= 'D';

        $model = new JobModel();
        return $model->getJobList($query->city, $query->type);
    }

    protected function afterGetData(DataQuery $query, &$data) {
        // 1. 指定上下文 -> 2. 装载排序插件 -> 3. 进行排序
        ListSortEngine::context($query)
            ->load('BusinessListSort')->load('RecommendListSort')->load('TopListSort')
            ->sort($data);

        /**
        // 进行运营排序
        $scores = array();
        $weights = array();

        foreach ($data as $it) {
            $scores[] = $it['score'];
            $weights[] = $it['weight'];
        }

        // 多维度排序
        array_multisort($scores, SORT_DESC, $weights, SORT_DESC, $data);
         */
    }

    protected function getCacheKey(DataQuery $query) {
        return 'job_list_city__' . $query->city . '_type_' . $query->type;
    }
}
