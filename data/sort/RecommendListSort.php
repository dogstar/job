<?php
require_once dirname(__FILE__) . '/ListSort.php';

/** 
 * 推荐算法排序
 */
class RecommendListSort extends ListSort {
    public function sort($list) {
        $oldIds = array();
        $oldList = array(); // 以招聘id为索引的列表

        foreach ($list as $it) {
            $oldIds[] = $it['id'];
            $oldList[$it['id']] = $it;
        }

        $newIds = $this->callRecommendSortApi($oldIds);

        $newList = array();
        foreach ($newIds as $id) {
            // 场景1：推荐算法有的，原列表没有的，则忽略
            if (!isset($oldList[$id])) {
                continue;
            }

            // 场景2：推荐算法有的，原列表也有的，则按推荐算法排序
            $newList[] = $oldList[$id];

            // 划除
            unset($oldList[$id]);
        }

        // 场景3：推荐算法没有的，原列表有的，则置后
        $newList = array_merge($newList, $oldList);

        return array_values($newList);
    }

    // 调用推荐算法接口服务
    public function callRecommendSortApi($ids) {
        // 暂且模拟，奇数在前，偶数在后

        $odd = array();  // 奇数
        $even = array(); // 偶数

        foreach ($ids as $id) {
            if ($id % 2 == 0) {
                $even[] = $id;
            } else {
                $odd[] = $id;
            }
        }

        return array_merge($odd, $even);
    }
}

