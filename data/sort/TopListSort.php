<?php
require_once dirname(__FILE__) . '/ListSort.php';

/** 
 * 活动排序
 */
class TopListSort extends ListSort {
    public function sort($list) {
        if ($this->query->topId <= 0) {
            return $list;
        }

        $topItem = null;
        foreach ($list as $key => $it) {
            if ($it['id'] == $this->query->topId) {
                $topItem = $it;
                unset($list[$key]);
            }
        }

        if ($topItem !== null) {
            array_unshift($list, $topItem);
        }

        return $list;
    }
}

