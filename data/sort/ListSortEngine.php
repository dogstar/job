<?php
/**
 * 列表页排序引擎
 */
class ListSortEngine {

    protected $query = null;
    protected $plugins = array();

    public static function context($query) {
        return new self($query);
    }

    protected function __construct($query) {
        $this->query = $query;
    }

    public function load($plugin) {
        $this->plugins[] = $plugin;
        return $this;
    }

    public function sort(&$list) {
        foreach ($this->plugins as $plugin) {
            $obj = new $plugin($this->query);
            $list = $obj->sort($list);    

            // 追加中间排序结果的纪录
            foreach ($list as $id => &$itRef) {
                if (!isset($itRef['__SEQ__'])) {
                    $itRef['__SEQ__'] = $id;
                } else {
                    $itRef['__SEQ__'] .= '-' . $id;
                }
            }
            unset($itRef);
        }

        return $list;
    }
}

