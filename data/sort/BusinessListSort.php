<?php
require_once dirname(__FILE__) . '/ListSort.php';

/** 
 * 运营排序
 */
class BusinessListSort extends ListSort {
    public function sort($list) {
        // 进行运营排序
        $scores = array();
        $weights = array();

        foreach ($list as $it) {
            $scores[] = $it['score'];
            $weights[] = $it['weight'];
        }

        // 多维度排序
        array_multisort($scores, SORT_DESC, $weights, SORT_DESC, $list);

        return $list;
    }
}

