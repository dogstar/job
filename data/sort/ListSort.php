<?php
/** 
 * 排序接口规约
 */
abstract class ListSort {

    protected $query;

    public function __construct($query) {
        $this->query = $query;
    }

    abstract public function sort($list);
}

