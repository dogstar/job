<?php
require_once dirname(__FILE__) . '/dataprovider/LightWeightDataProvider.php';
require_once dirname(__FILE__) . '/../model/CategoryModel.php';

class CategoryData extends LightWeightDataProvider {

    protected function doGetData(DataQuery $query, &$trace = '') {
        // 全球追踪器：数据库
        $trace .= 'D';

        $model = new CategoryModel();
        return $model->getAllJobType();
    }

    protected function getCacheKey(DataQuery $query) {
        return 'category_job_type';
    }
}
