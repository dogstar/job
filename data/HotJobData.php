<?php
require_once dirname(__FILE__) . '/dataprovider/RealTimeDataProvider.php';
require_once dirname(__FILE__) . '/../model/JobModel.php';

class HotJobData extends RealTimeDataProvider {

    protected function doGetData(DataQuery $query, &$trace = '') {
        // 全球追踪器：远程接口
        $trace .= 'A';

        $model = new JobModel();
        return $model->getHotJob($query->city);
    }
}
