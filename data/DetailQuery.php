<?php
require_once dirname(__FILE__) . '/dataprovider/DataQuery.php';

class DetailQuery extends DataQuery {

    public $id;

    public function __construct() {
        $this->id = intval($_GET['id']);
    }
}
