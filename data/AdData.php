<?php
require_once dirname(__FILE__) . '/dataprovider/LightWeightDataProvider.php';
require_once dirname(__FILE__) . '/../model/AdModel.php';

class AdData extends LightWeightDataProvider {

    protected function doGetData(DataQuery $query, &$trace = '') {
        // 全球追踪器：数据库
        $trace .= 'D';

        $model = new AdModel();
        return $model->getTopBanner($query->city);
    }

    protected function getCacheKey(DataQuery $query) {
        return 'top_banner_ad_' . $query->city;
    }
}
