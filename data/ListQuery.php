<?php
require_once dirname(__FILE__) . '/dataprovider/DataQuery.php';

class ListQuery extends DataQuery {
    // 所在城市，如：gz＝广州，sz=深圳
    public $city;

    // 岗位类型，如：php, java
    public $type;

    // 置顶ID
    public $topId;

    public function __construct() {
        // 默认城市
        $this->city = 'gz';

        // 预览时指定的城市
        if (!empty($_GET['city'])) {
            $this->city = $_GET['city'];
        }

        $this->type = $_GET['type'];

        $this->topId = isset($_GET['top_id']) ? $_GET['top_id'] : 0;

        parent::__construct();
    }
}
