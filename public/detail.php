<?php

require dirname(__FILE__) . '/../data/DetailQuery.php';
require dirname(__FILE__) . '/../controller/DetailFactory.php';

// 三种写法

DetailFactory::createController(new DetailQuery())->render();

die();

$query = new DetailQuery();

DetailFactory::createController($query)->render();

