<?php

class CategoryModel /** extends Model **/{

    public function getAllJobType() {
        return array(
            // 后端开发
            array(
                array('type' => '后端开发', 'link' => '/list-backend.html', 'active' => true),
                array('type' => 'Java',     'link' => '/list-java.html'),
                array('type' => 'PHP',      'link' => '/list-php.html'),
                array('type' => 'Python',   'link' => '/list-python.html'),
                array('type' => 'Ruby',     'link' => '/list-ruby.html'),
            ),
            // 前端开发
            array(
                array('type' => '前端开发',   'link' => '/list-frontend.html', 'active' => true),
                array('type' => 'Web前端',    'link' => '/list-web.html'),
                array('type' => 'Javascript', 'link' => '/list-javascript.html'),
                array('type' => 'Html5',      'link' => '/list-html5.html'),
            ),
            // 移动开发
            array(
                array('type' => '移动开发', 'link' => '/list-mobile.html', 'active' => true),
                array('type' => 'iOS',      'link' => '/list-ios.html'),
                array('type' => 'Android',  'link' => '/list-android.html'),
                array('type' => 'WP',       'link' => '/list-wp.html'),
            ),
        );
    }
}
