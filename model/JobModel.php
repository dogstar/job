<?php

class JobModel /** extends Model **/{

    public function getHotJob($city) {
        return array(
            array(
                'job_name' => 'Java高级开发工程师', 
                'job_snapshot' => '10k-20k / 广州 / 经验5-10年 / 本科及以上 / 全职', 
                'link' => '/detail-1.html',
            ),
            array(
                'job_name' => '资深PHP开发工程师', 
                'job_snapshot' => '15k-30k / 广州 / 经验5-10年 / 本科及以上 / 全职', 
                'link' => '/detail-2.html',
            ),
            array(
                'job_name' => 'Ruby开发工程师', 
                'job_snapshot' => '8k-15k / 广州 / 经验1-3年 / 本科及以上 / 全职', 
                'link' => '/detail-3.html',
            ),
        );
    }

    public function getJobList($city, $type) {
        return array(
            array(
                'id' => 21,
                'job_name' => 'PHP软件开发工程师', 
                'job_snapshot' => '6k-8k / 经验1年以下 / 大专 / 全职', 
                'score' => 8.8,
                'weight' => 90,
            ),
            array(
                'id' => 22,
                'job_name' => '高级PHP工程师', 
                'job_snapshot' => '12k-23k / 经验3-5年 / 本科 / 全职', 
                'score' => 9.2,
                'weight' => 80,
            ),
            array(
                'id' => 23,
                'job_name' => '资深PHP开发工程师', 
                'job_snapshot' => '15k-30k / 经验5-10年 / 本科及以上 / 全职', 
                'score' => 8.8,
                'weight' => 92,
            ),
            array(
                'id' => 24,
                'job_name' => 'PHP程序员', 
                'job_snapshot' => '4k-5k / 广州 / 经验1年以下 / 大专 / 全职', 
                'score' => 8.6,
                'weight' => 95,
            ),
            array(
                'id' => 25,
                'job_name' => '高级PHP开发工程师', 
                'job_snapshot' => '14k-20k / 广州 / 经验1-3年 / 本科 / 全职', 
                'score' => 9.3,
                'weight' => 88,
            ),
        );
    }
}
