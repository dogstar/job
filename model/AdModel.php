<?php

class AdModel /** extends Model **/{

    public function getTopBanner($city) {
        // 假设从数据库获取的数据如下……
        return array(
            'title' => '技术类校招专场', 
            'desc' => '专为应届毕业生准备的招聘专场，快来看看吧！', 
            'button' => '马上进入',
            'link' => '/act-1.html', 
        );
    }
}
