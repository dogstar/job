<?php

abstract class DetailController {

    protected $query;
    protected $miniData;

    public function __construct($query, $miniData) {
        $this->query = $query;
        $this->miniData = $miniData;
    }

    abstract public function render();
}
