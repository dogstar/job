<?php
require_once dirname(__FILE__) . '/../data/ListQuery.php';
require_once dirname(__FILE__) . '/../data/JobListData.php';

class ListController {

    public function show() {
        // 列表页查询对象
        $listQuery = new ListQuery();

        $jobListData = new JobListData();
        $job = $jobListData->getData($listQuery);

        // 视图渲染
        include(dirname(__FILE__) . '/../view/list.html');
    }
}
