<?php
require_once dirname(__FILE__) . '/../DetailController.php';
require_once dirname(__FILE__) . '/../../data/MoreDetailData.php';

class DetailSchoolController extends DetailController {

    public function render() {
        $moreDetailData = new MoreDetailData();
        $moreData = $moreDetailData->getData($this->query);

        $data = array_merge($this->miniData, $moreData);

        include(dirname(__FILE__) . '/../../view/detail_school.html');
    }
}
