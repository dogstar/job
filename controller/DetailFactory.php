<?php
require_once dirname(__FILE__) . '/../data/MiniDetailData.php';
require_once dirname(__FILE__) . '/detail/DetailSchoolController.php';
require_once dirname(__FILE__) . '/detail/DetailSocialController.php';
require_once dirname(__FILE__) . '/detail/DetailDefaultController.php';

class DetailFactory {

    public static function createController($query) {
        $miniDetailData = new MiniDetailData();
        $miniData = $miniDetailData->getData($query);

        if ($miniData['type'] == 'school') {
            // 校招
            return new DetailSchoolController($query, $miniData);
        } else if ($miniData['type'] == 'social') {
            // 社招
            return new DetailSocialController($query, $miniData);
        }

        // 默认显示
        return new DetailDefaultController($query, $miniData);
    }
}
