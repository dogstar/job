<?php
require_once dirname(__FILE__) . '/../data/IndexQuery.php';
require_once dirname(__FILE__) . '/../data/AdData.php';
require_once dirname(__FILE__) . '/../data/CategoryData.php';
require_once dirname(__FILE__) . '/../data/HotJobData.php';

class IndexController {

    public function index() {
        // 首页查询参数对象
        $query = new IndexQuery();

        // 全球追踪器
        $trace = 'Index';

        // 顶部广告
        $trace .= '|AdData-';
        $adData = new AdData();
        $ad = $adData->getData($query, $trace);

        // 职位分类
        $trace .= '|CategoryData-';
        $cateData = new CategoryData();
        $category = $cateData->getData($query, $trace);

        // 热门职位
        $trace .= '|HotJobData-';
        $hotData = new HotJobData();
        $job = $hotData->getData($query, $trace);

        // 通过头部输出全球追踪器
        header('TRACE: ' . $trace);

        // 视图渲染
        include(dirname(__FILE__) . '/../view/index.html');
    }
}
